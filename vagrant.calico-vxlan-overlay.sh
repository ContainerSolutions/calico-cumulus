#!/bin/sh

vagrant ssh k8s-master-l1-1 -c "kubectl apply -f calico-vxlan-overlay/calicoctl.yaml"
vagrant ssh k8s-master-l1-1 -c "kubectl apply -f calico-vxlan-overlay/calico.yaml"

#sleep 120

#vagrant ssh k8s-master-l1-1 -c "kubectl exec -i -n kube-system calicoctl -- calicoctl apply -f - < calico-vxlan-overlay/calico.nodes.yaml"
#vagrant ssh k8s-master-l1-1 -c "kubectl exec -i -n kube-system calicoctl -- calicoctl apply -f - < calico-vxlan-overlay/calico.bgpconfig.yaml"

#alias calicoctl="kubectl exec -i -n kube-system calicoctl /calicoctl -- "
