#!/bin/sh

vagrant ssh k8s-master-l1-1 -c "kubectl label nodes k8s-master-l1-1 'asnum=65101'"
vagrant ssh k8s-master-l1-1 -c "kubectl label nodes k8s-node-l1-1   'asnum=65101'"
vagrant ssh k8s-master-l1-1 -c "kubectl label nodes k8s-node-l1-2   'asnum=65101'"
vagrant ssh k8s-master-l1-1 -c "kubectl label nodes k8s-node-l2-1   'asnum=65102'"
vagrant ssh k8s-master-l1-1 -c "kubectl label nodes k8s-node-l2-2   'asnum=65102'"

vagrant ssh k8s-master-l1-1 -c "kubectl apply -f calico-with-peering/calicoctl.yaml"
vagrant ssh k8s-master-l1-1 -c "kubectl apply -f calico-with-peering/calico.yaml"

sleep 120

vagrant ssh k8s-master-l1-1 -c "kubectl exec -i -n kube-system calicoctl -- calicoctl apply -f - < calico-with-peering/calico.nodes.yaml"
vagrant ssh k8s-master-l1-1 -c "kubectl exec -i -n kube-system calicoctl -- calicoctl apply -f - < calico-with-peering/calico.bgpconfig.yaml"

#alias calicoctl="kubectl exec -i -n kube-system calicoctl /calicoctl -- "
